using System;
using System.Numerics;

namespace SpaceButtleGame.TestProject;

public class MovableObject : IMovable
{
    internal Vector2 Position { get; set; }
    internal Vector2 Velocity { get; set; }
    internal bool CanMove { get; set; }

    public MovableObject()
    {
        Position = new Vector2(float.MaxValue, float.MaxValue);
        Velocity = new Vector2(float.MaxValue, float.MaxValue);
        CanMove = true;
    }

    public Vector2 GetPosition()
    {
        return Position;
    }

    public Vector2 GetVelocity()
    {
        return Velocity;
    }

    public void SetPosition(Vector2 newValue)
    {
        if (!CanMove)
            throw new Exception("Can't move");
        Position = newValue;
    }
}