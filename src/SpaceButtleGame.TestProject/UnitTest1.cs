using System;
using System.Numerics;
using NUnit.Framework;

namespace SpaceButtleGame.TestProject;

public class Tests
{
    private IMovable _movableObject;
    private Move _move;
    
    [SetUp]
    public void Setup()
    {
        _movableObject = new MovableObject();

        _move = new Move(_movableObject);
    }

    [Test,
    Description("Реализован тест: Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3)\n" +
                "движение меняет положение объекта на (5, 8)")]
    public void Test5()
    {
        ((MovableObject)_movableObject).Position = new Vector2(12, 5);
        ((MovableObject)_movableObject).Velocity = new Vector2(-7, 3);
        _move.Execute();
        Assert.AreEqual(new Vector2(5f,8f), _movableObject.GetPosition(), "New position");
    }

    [Test,
     Description("Реализован тест: Попытка сдвинуть объект, \n" +
                 "у которого невозможно прочитать положение объекта в пространстве,, приводит к ошибке")]
    public void Test6()
    {
        Assert.Throws<Exception>(() =>
        {
            _move.Execute();
        }, "Throws");
    }
    
    [Test,
     Description("Реализован тест: Попытка сдвинуть объект, \n" +
                 "у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке")]
    public void Test7()
    {
        ((MovableObject)_movableObject).Position = new Vector2(12, 5);
        Assert.Throws<Exception>(() =>
        {
            _move.Execute();
        }, "Throws");
    }
    
    [Test,
     Description("Реализован тест: Попытка сдвинуть объект, \n" +
                 "у которого невозможно изменить положение в пространстве, приводит к ошибке")]
    public void Test8()
    {
        ((MovableObject)_movableObject).Position = new Vector2(12, 5);
        ((MovableObject)_movableObject).Velocity = new Vector2(-7, 3);
        ((MovableObject) _movableObject).CanMove = false;
        Assert.Throws<Exception>(() =>
        {
            _move.Execute();
        }, "Throws");
    }
}