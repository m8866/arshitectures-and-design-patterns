﻿using System.Numerics;

namespace SpaceButtleGame;

public interface IMovable
{
    Vector2 GetPosition();
    Vector2 GetVelocity();
    void SetPosition(Vector2 newValue);
}