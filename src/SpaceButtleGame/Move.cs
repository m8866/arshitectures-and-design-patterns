using System.Numerics;

namespace SpaceButtleGame;

public class Move
{
    private readonly IMovable _movable;

    public Move(IMovable movable)
    {
        _movable = movable;
    }

    public void Execute()
    {
        var position = _movable.GetPosition();
        if (position.X.Equals(float.MaxValue) &&  position.Y.Equals(float.MaxValue))
            throw new Exception("position");

        var velocity = _movable.GetVelocity();
        if (velocity.X.Equals(float.MaxValue) &&  velocity.Y.Equals(float.MaxValue))
            throw new Exception("velocity");
        
        _movable.SetPosition(Vector2.Add(position, velocity));
    }
}