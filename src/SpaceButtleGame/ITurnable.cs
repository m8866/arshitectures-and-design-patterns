namespace SpaceButtleGame;

public interface ITurnable
{
    int GetAngle();
    void Turn(int angle);
}