namespace SpaceButtleGame;

public class Turn
{
    private readonly ITurnable _turnable;

    public Turn(ITurnable turnable)
    {
        _turnable = turnable;
    }

    public void Execute()
    {
        _turnable.Turn(_turnable.GetAngle());
    }
}